// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

//Interfaz para la clase CMundoCliente class.


class CMundoCliente
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	int fd;
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	DatosMemCompartida datos_compartidos;
	DatosMemCompartida* puntero_datos_compartidos;
	
	Socket servidor;

	int fifo_servidor;
	int fifo_cliente;
	int puntos1;
	int puntos2;
	
	typedef struct puntuaciones
	{
		int jugador1;
		int jugador2;
		int lastwinner;
	}puntuaciones;
	
	puntuaciones puntos;	
};

#endif // !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
