#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
//#include <iostream>
//#include "puntuaciones.h"

typedef struct puntuaciones
{
	int jugador1;
	int jugador2;
	int lastwinner;
}puntuaciones;


int main()
{
    int fd;
    puntuaciones puntos;

    // Crear FIFO
    if (mkfifo("FIFO_LOGGER", 0600)<0) {
        perror("No puede crearse el FIFO");
        return 1;
    }

    // Abro el FIFO
    if ((fd=open("FIFO_LOGGER", O_RDONLY))<0) {
        perror("No puede abrirse el FIFO");
        return 1;
    }

    while (read(fd, &puntos, sizeof(puntos))==sizeof(puntos))  {
        if(puntos.lastwinner==1){
        printf("Jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos.jugador1+1);
        }
        else if(puntos.lastwinner==2){
        printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2+1);
        }        
    }

    close(fd);

    unlink("FIFO_LOGGER");

    return(0);
}
