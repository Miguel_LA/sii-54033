#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main()
{
	int fd;
	DatosMemCompartida *puntero_datos_compartidos;
	fd=open("datosCompartidos", O_RDWR);
	if(fd<0)
	{
		perror("Error apertura fichero.");
		exit(1);
	}
	
	
	puntero_datos_compartidos=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	
	if ((puntero_datos_compartidos=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)))==MAP_FAILED)
	{
		perror("Error en proyeccion fichero.");
		close(fd);
		return(1);
	}
	close(fd);
	
	while(1)
	{
		if(puntero_datos_compartidos->esfera.centro.y < puntero_datos_compartidos->raqueta1.getCentro().y)
		{
			puntero_datos_compartidos->accion=-1;
		}
		else if(puntero_datos_compartidos->esfera.centro.y == puntero_datos_compartidos->raqueta1.getCentro().y)
		{
			puntero_datos_compartidos->accion=0;
		}
		else if(puntero_datos_compartidos->esfera.centro.y > puntero_datos_compartidos->raqueta1.getCentro().y)
		{
			puntero_datos_compartidos->accion=1;
		}	

		usleep(25000);
	}
	
	munmap(puntero_datos_compartidos, sizeof(DatosMemCompartida));
	unlink("datosCompartidos");

	return 1;
}
