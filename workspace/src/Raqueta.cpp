// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	posicion.x=0.0f;
	posicion.y=0.0f;
	velocidad.x=0.0f;
	velocidad.y=0.0f;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1+=velocidad.y*t;
	y2+=velocidad.y*t;
}

Vector2D Raqueta::getCentro()
{

	Vector2D resultado;
	resultado.x= (abs(x1)-abs(x2))/2;
	resultado.y= (abs(y1)-abs(y2))/2;
	
	return resultado;
}
